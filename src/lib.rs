extern crate sdl2;
extern crate gl;
extern crate cgmath;
extern crate stb_image;

pub mod renderer;
pub mod inputstate;
mod glwrapper;

use renderer::{Renderer};
use inputstate::{InputState, Key};
use std::time::{SystemTime};

pub enum GraphicsApi {
    OpenGL,
    DirectX,
    Vulkan,
    Metal
}

pub struct Engine {
    _graphics_api: GraphicsApi,
    pub renderer: Renderer,
    pub input_state: InputState
}

impl Engine {
    pub fn init(graphics_api: GraphicsApi) -> Result<Engine, String> {
        // At the moment only OpenGL is supported
        match graphics_api {
            GraphicsApi::OpenGL => {
                // Initialize sdl2
                let sdl = sdl2::init().unwrap();
                let video_subsystem = sdl.video().unwrap();

                // Get the event pump
                let mut event_pump = sdl.event_pump().unwrap();
                event_pump.enable_event(sdl2::event::EventType::KeyDown);

                // Initialize the Renderer
                let renderer = Renderer::create(video_subsystem, 1280, 720);

                return Ok(Engine {
                    _graphics_api: graphics_api,
                    renderer: renderer,
                    input_state: InputState::new(event_pump)
                });
            },
            _ => {
                Err(String::from("Unsupported graphics API. Only OpenGL is currently supported"))
            }
        }
    }

    pub fn run<F>(&mut self, update_function: &mut F) where F: FnMut(f64, &mut Engine) {
        let mut current_time = SystemTime::now();

        'main: loop {
            let start = SystemTime::now();
            let delta = start.duration_since(current_time).unwrap();
            current_time = start;
            let dt = delta.as_secs() as f64 + delta.subsec_nanos() as f64 * 1e-9;

            // Update the game
            self.input_state.update_events();
            update_function(dt, self);

            if self.input_state.should_close {
                break 'main;
            }

            // Update animations and render everything
            self.renderer.update_animations(dt);
            self.renderer.render();
        }
    }

    pub fn exit(&mut self) {
        self.input_state.should_close = true;
    }
}
