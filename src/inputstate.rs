use sdl2;

pub enum Key {
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3,
    Escape = 4,
    Max = 5
}

pub struct InputState {
    event_pump: sdl2::EventPump,
    keys_down: [bool; Key::Max as usize],
    pub should_close: bool
}

impl InputState {
    pub fn new(event_pump: sdl2::EventPump) -> InputState {
        InputState { event_pump: event_pump, keys_down: [false; Key::Max as usize], should_close: false }
    }

    pub fn update_events(&mut self) {
        for event in self.event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit {..} => { self.should_close = true },
                sdl2::event::Event::KeyDown { keycode, .. } => {
                    if let Some(code) = keycode {
                        match code {
                            sdl2::keyboard::Keycode::Escape => {
                                self.keys_down[Key::Escape as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Up => {
                                self.keys_down[Key::Up as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Down => {
                                self.keys_down[Key::Down as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Left => {
                                self.keys_down[Key::Left as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Right => {
                                self.keys_down[Key::Right as usize] = true;
                            },
                            _ => {}
                        }
                    }
                }

                sdl2::event::Event::KeyUp { keycode, .. } => {
                    if let Some(code) = keycode {
                        match code {
                            sdl2::keyboard::Keycode::Up => {
                                self.keys_down[Key::Up as usize] = false;
                            },
                            sdl2::keyboard::Keycode::Down => {
                                self.keys_down[Key::Down as usize] = false;
                            },
                            sdl2::keyboard::Keycode::Left => {
                                self.keys_down[Key::Left as usize] = false;
                            },
                            sdl2::keyboard::Keycode::Right => {
                                self.keys_down[Key::Right as usize] = false;
                            },
                            _ => {}
                        }
                    }
                },
                _ => {},
            }
        }
    }

    pub fn key_down(&self, keycode: Key) -> bool {
        self.keys_down[keycode as usize]
    }
}
